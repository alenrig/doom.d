;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; refresh' after modifying this file!


;; ===== Main =====
;; These are used for a number of things, particularly for GPG configuration,
;; some email clients, file templates and snippets.
(setq user-full-name "Aleksandr Gorokhov"
      user-mail-address "agorokhov94@gmail.com")

;; From modules/appearance/fonts
;; This is un-tracked module, specially designed for every uniq machine
(require 'fonts "~/.doom.d/fonts.el")

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. These are the defaults.
(setq doom-theme 'dracula)

;; Default projects path
(setq projectile-project-search-path '("~/code/"))

;; ===== Org =====
(after! org
  (require 'org-tempo)
  (set-popup-rule! "^ \\*Org tags" :side 'bottom :size 0.80 :select t :ttl nil)
  (setq org-directory "~/Documents/org"
        org-agenda-files (directory-files-recursively org-directory "\.org$")
        org-todo-keywords '((sequence "TODO(t)" "WAITING(w)" "INPROGRESS(p)" "|" "DONE(d)" "CANCELED(c)"))
        org-todo-keyword-faces
        '(("TODO" :foreground "#7c7c75" :weight normal :underline t)
          ("WAITING" :foreground "#9f7efe" :weight normal :underline t)
          ("INPROGRESS" :foreground "#454aff" :weight normal :underline t)
          ("DONE" :foreground "#50a14f" :weight normal :underline t)
          ("CANCELLED" :foreground "#ff6480" :weight normal :underline t)
         )
        org-fontify-done-headline t))

;; --- Org-super-agenda ---
(use-package! org-super-agenda
  :after org-agenda
  :init
  (setq org-super-agenda-groups '(
                                  (:name "Today" :time-grid t :scheduled today)
                                  (:name "Due today" :deadline today :order 9)
                                  (:name "Overdue" :deadline past :order 8)
                                  (:name "Important" :priority "A" :order 7)
                                  (:name "Personal" :habit t :tag ("personal" "raspberry") :order 6)
                                  (:name "Work" :tag "work" :order 5)
                                  (:todo "WAITING" :order 2)
                                  (:name "Other" :priority<= "B" :order 1))
        org-super-agenda-header-separator "*")
  :config
  (org-super-agenda-mode))

;; --- Org-capture ---
(after! org-capture
  (setq org-capture-templates '(
                                ("t" "My TODOs" entry (file+headline "todo.org" "Inbox")
                                 "* TODO %?\n%a")
                                ("j" "My Journal" entry (file+datetree "journal.org")
                                 "* %U\n%?")
                                ("n" "My Notes" entry (file+headline "notes.org" "Inbox")
                                 "* %?\n%a"))))

(add-hook 'text-mode-hook 'turn-on-auto-fill)

;; ===== Lang =====

(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)

(setq-default display-line-numbers-type `relative)
(setq-default display-fill-column-indicator-column 119)
(add-hook 'prog-mode-hook 'display-fill-column-indicator-mode)

;; --- Python ---
;; - Jedi -
(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:complete-on-dot t)

;; == Mac ===
(cond
  ((string-equal system-type "darwin")
   (progn
     (exec-path-from-shell-initialize)
     (exec-path-from-shell-copy-envs '("PATH"))
     (setq jedi:environment-root "~/.emacs.d/var/jedi/venv"))))


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', where Emacs
;;   looks when you load packages with `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.
